/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj2.command.PIDSubsystem;
import edu.wpi.first.wpilibj.PWMVictorSPX;
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.Encoder;
import frc.robot.Constants;

public class BallShooterRight extends PIDSubsystem {
  /**
   * Creates a new BallShooter.
   */
  private static BallShooterRight instance;

  private final PWMVictorSPX rightShooter = new PWMVictorSPX(Constants.SHOOTER_MOTOR2);
  private final SimpleMotorFeedforward m_shooterFeedforward =
      new SimpleMotorFeedforward(Constants.ShooterkSVolts, Constants.ShooterkVVoltSecondsPerRotation);
  private final Encoder m_shooterEncoder1 = new Encoder(Constants.kEncoderPorts[2], Constants.kEncoderPorts[3]);


  public BallShooterRight() {
    super(new PIDController(Constants.ShooterkP, Constants.ShooterkI, Constants.ShooterkD));
    getController().setTolerance(Constants.kShooterToleranceRPS);
    setSetpoint(Constants.kShooterTargetRPM);
  }

  public static BallShooterRight getInstance() {
    if (instance == null) {
        instance = new BallShooterRight();
    }

    return instance;
}

  public boolean atSetpoint() {
    return m_controller.atSetpoint();
  }

  public void setSetpoint(double value){
      m_controller.setSetpoint(value);
  }

  @Override
  public void useOutput(double output, double setpoint) {
    rightShooter.setVoltage(output + m_shooterFeedforward.calculate(setpoint));
  }

  @Override
  public double getMeasurement() {
    // Return the process variable measurement here
    return m_shooterEncoder1.getRate();
  }
}
