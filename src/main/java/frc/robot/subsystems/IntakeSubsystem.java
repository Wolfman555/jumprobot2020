/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.PWMVictorSPX;
import frc.robot.Constants;

public class IntakeSubsystem extends SubsystemBase {
  /**
   * Creates a new IntakeSubsystem.
   */
  PWMVictorSPX intakeMotor1 = new PWMVictorSPX(Constants.INTAKE_MOTOR1);
  PWMVictorSPX intakeMotor2 = new PWMVictorSPX(Constants.INTAKE_MOTOR2);
  private static IntakeSubsystem instance;
  public IntakeSubsystem() {

  }

  public static IntakeSubsystem getInstance() {
        if (instance == null) {
            instance = new IntakeSubsystem();
        }

        return instance;
    }

  public void setIntake(float speed){
      intakeMotor1.set(speed);
      intakeMotor2.set(speed);
  }

  @Override                                                                                      
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
