/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj2.command.PIDSubsystem;
import edu.wpi.first.wpilibj.PWMVictorSPX;
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.Encoder;
import frc.robot.Constants;

public class BallShooterLeft extends PIDSubsystem {
  /**
   * Creates a new BallShooter.
   */
  private static BallShooterLeft instance;

  private final PWMVictorSPX leftShooter = new PWMVictorSPX(Constants.SHOOTER_MOTOR1);
  private final SimpleMotorFeedforward m_shooterFeedforward =
      new SimpleMotorFeedforward(Constants.ShooterkSVolts, Constants.ShooterkVVoltSecondsPerRotation);
  private final Encoder m_shooterEncoder1 = new Encoder(Constants.kEncoderPorts[0], Constants.kEncoderPorts[1]);


  public BallShooterLeft() {
    super(new PIDController(Constants.ShooterkP, Constants.ShooterkI, Constants.ShooterkD));
    getController().setTolerance(Constants.kShooterToleranceRPS);
    leftShooter.setInverted(true);
    setSetpoint(Constants.kShooterTargetRPM);
  }

  public static BallShooterLeft getInstance() {
    if (instance == null) {
        instance = new BallShooterLeft();
    }

    

    return instance;
}

public void setSetpoint(double value){
  setSetpoint(value);
}

  @Override
  public void useOutput(double output, double setpoint) {
    leftShooter.setVoltage(output + m_shooterFeedforward.calculate(setpoint));
  }

  @Override
  public double getMeasurement() {
    // Return the process variable measurement here
    return m_shooterEncoder1.getRate();
  }
}
