package frc.robot;

public class Constants {
    public static final int DRIVETRAIN_FRONT_LEFT_ANGLE_MOTOR = 2; // CAN
    public static final int DRIVETRAIN_FRONT_LEFT_ANGLE_ENCODER = 1; // Analog
    public static final int DRIVETRAIN_FRONT_LEFT_DRIVE_MOTOR = 1; // CAN

    public static final int DRIVETRAIN_FRONT_RIGHT_ANGLE_MOTOR = 7; // CAN
    public static final int DRIVETRAIN_FRONT_RIGHT_ANGLE_ENCODER = 2; // Analog
    public static final int DRIVETRAIN_FRONT_RIGHT_DRIVE_MOTOR = 8; //CAN

    public static final int DRIVETRAIN_BACK_LEFT_ANGLE_MOTOR = 4; // CAN
    public static final int DRIVETRAIN_BACK_LEFT_ANGLE_ENCODER = 0; // Analog
    public static final int DRIVETRAIN_BACK_LEFT_DRIVE_MOTOR = 3; // CAN

    public static final int DRIVETRAIN_BACK_RIGHT_ANGLE_MOTOR = 6; // CAN
    public static final int DRIVETRAIN_BACK_RIGHT_ANGLE_ENCODER = 3; // Analog
    public static final int DRIVETRAIN_BACK_RIGHT_DRIVE_MOTOR = 5; // CAN

    public static final int INTAKE_MOTOR1 = 2;
    public static final int INTAKE_MOTOR2 = 3;

    public static final int SHOOTER_MOTOR1 = 0;
    public static final int SHOOTER_MOTOR2 = 1;

    public static final double ShooterkSVolts = 0.0;
    public static final double ShooterkVVoltSecondsPerRotation = 0.0;

    public static final double ShooterkP = 0.0;
    public static final double ShooterkI = 0.0;
    public static final double ShooterkD = 0.0;

    public static final double kShooterToleranceRPS = 0.0;

    public static final int[] kEncoderPorts = {0,1,2,3};

    public static double kShooterTargetRPM = 4000;

}
