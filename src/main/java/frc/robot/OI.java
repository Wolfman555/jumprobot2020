package frc.robot;

import frc.robot.subsystems.*;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import edu.wpi.first.wpilibj.command.InstantCommand;
import frc.robot.commands.*;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.CommandScheduler;


public class OI {
    /*
       Add your joysticks and buttons here
     */
    private Joystick primaryJoystick = new Joystick(0);
    private final AutoAlign m_autoalign = new AutoAlign(DrivetrainSubsystem.getInstance());

    

    Timer timer = new Timer();


    public OI() {

       // CommandScheduler.getInstance().setDefaultCommand(DrivetrainSubsystem.getInstance(), new DriveCommand());

        // Back button zeroes the drivetrain
        new JoystickButton(primaryJoystick, 6).whenPressed(() -> DrivetrainSubsystem.getInstance().resetGyroscope());

        new JoystickButton(primaryJoystick, 2).whenPressed(new EnableShooter());

        new JoystickButton(primaryJoystick, 1).whenPressed(new DisableShooter());
        
        new JoystickButton(primaryJoystick, 3).whileHeld(m_autoalign);



    }

    public void autonomous(){
        timer.stop();
        timer.reset();
        timer.start();
        new EnableShooter();
        while(timer.get() < 3){
             DrivetrainSubsystem.getInstance().drive(new Translation2d(1,0), 0, true);
        }
        timer.reset();
        while(timer.get() < 4){
            DrivetrainSubsystem.getInstance().drive(new Translation2d(1,1), 4, true);
        }
    }

    public Joystick getPrimaryJoystick() {
        return primaryJoystick;
    }
}
