package frc.robot;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;


public class Robot extends TimedRobot {
    private static OI oi;

    public static OI getOi() {
        return oi;
    }

    @Override
    public void robotInit() {
        oi = new OI();
    }

    @Override
    public void robotPeriodic() {
        Scheduler.getInstance().run();
        Constants.kShooterTargetRPM = oi.getPrimaryJoystick().getRawAxis(1) * 4000;
    }

    public void autonomousInit(){
        oi.autonomous();
    }

    public void autonomousPeriodic(){
    }

    public void testInit(){

    }

    public void testPeriodic(){

    }

}
